/**
 * Created by Freakk on 21/12/2014.
 */
var Key = {
    _pressed: {},

    LEFT    :   37,
    UP      :   38,
    RIGHT   :   39,
    DOWN    :   40,
    SPACE   :   32,
    L_SHIFT :   16,
    L_CTRL  :   17,
    ALT     :   18,
    D       :   68,
    ONE     :   49,
    TWO     :   50,

    isDown: function(keyCode) {
        return this._pressed[keyCode];
    },

    onKeydown: function(event) {
        this._pressed[event.keyCode] = true;
    },

    onKeyup: function(event) {
        delete this._pressed[event.keyCode];
        switch(event.keyCode){
            case this.D:
                Game.debug ? Game.debug = false : Game.debug = true; // Toggle Debug Mode
                break;
            case this.ONE:
                initPlayer(playerObjects.xw);
                break;
            case this.TWO:
                initPlayer(playerObjects.mf);
                break;

        }
    },
    analogX : 0,
    analogY : 0,
    initOnScreenKeys: function(){
        //var keyDebug = document.getElementById("key-debug");
        var keysToggle = document.getElementById("toggle-controls");
        var keys = document.getElementById("keys");
        //keysToggle.addEventListener("touchend", toggleKeys );

        keysToggle.addEventListener("click", toggleKeys );
        function toggleKeys(){
            if(getCSS("keys","display")=="block"){
                keys.style.display = "none";
            } else if(getCSS("keys","display")=="none"){
                keys.style.display = "block";
            }
        }
        var isMobile = {
            Android: function() {
                return /Android/i.test(navigator.userAgent);
            },
            BlackBerry: function() {
                return /BlackBerry/i.test(navigator.userAgent);
            },
            iOS: function() {
                return /iPhone|iPad|iPod/i.test(navigator.userAgent);
            },
            Windows: function() {
                return /IEMobile/i.test(navigator.userAgent);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
            }
        };

        if(isMobile.any()){
            keys.style.display = "block";
        }
        var stickLeft = document.getElementById("stick-left");
        var stickLeftSizeHalf = parseInt(getCSS("stick-left","width"))/2 ;
        var stickLeftCenterH = parseInt(getCSS("stick-left","left")) + parseInt(getCSS("stick-left","width"))/2 ;
        var stickLeftCenterV = Game.stage.height - (parseInt(getCSS("stick-left","bottom")) + parseInt(getCSS("stick-left","height"))/2 ) ;
        var intensityX = 0;
        var intensityY = 0;
        stickLeft.addEventListener("touchmove", function(evt){
                evt.preventDefault();
                var touchX = parseInt(event.touches[0].pageX);
                var touchY = parseInt(event.touches[0].pageY);
                Key.analogX = ( touchX - stickLeftCenterH )/stickLeftSizeHalf;
                Key.analogY = ( touchY - stickLeftCenterV )/stickLeftSizeHalf;
            }, false
        );

        stickLeft.addEventListener("touchend", function(evt){
                Key.analogX = 0;
                Key.analogY = 0;
            }, false
        );

        document.getElementById("toggle-player-1").addEventListener("click", function(evt){
                initPlayer(playerObjects.xw);
            }, false
        );
        document.getElementById("toggle-player-2").addEventListener("click", function(evt){
                initPlayer(playerObjects.mf);
            }, false
        );

        var keyFire = document.getElementById("key-fire");
        keyFire.addEventListener("touchstart", function(evt){evt.preventDefault(); Key.onKeydown({keyCode:Key.SPACE});}, false);
        keyFire.addEventListener("touchend", function(evt){evt.preventDefault(); Key.onKeyup({keyCode:Key.SPACE});}, false);


    }

};

function getCSS(element, property) {

    var elem = document.getElementById(element);
    var css = null;

    if(elem.currentStyle) {

        css = elem.currentStyle[property];


    } else if(window.getComputedStyle) {


        css = document.defaultView.getComputedStyle(elem, null).
            getPropertyValue(property);

    }

    return css;
};