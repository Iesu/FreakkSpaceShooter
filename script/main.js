/**
 * Created by Freakk on 20/12/2014.
 */

var canvas = null;
var context = null;
var bg_canvas = null;
var bg_img = new Image(); // storage for new background piece;
var bg_context = null;
var player;
var playerSpawnX = 600;
var playerSpawnY = 550;
var enemies = [];
var obstacles = [];
var bonuses = {};
var enemyObjects = null;
var t;
var points = 0;

window.onload = loadContent;

function init() {
    // Draw Backgorund
    bg_canvas = document.getElementById('background');
    bg_context = bg_canvas.getContext('2d');
    bg_img.onload = function () {
        bg_context.drawImage(bg_img, 0, 0);
    }
    bg_img.src = "images/bg2.jpg"

    // Main Stage
    canvas = document.getElementById('canvas');
    context = canvas.getContext('2d');

    // Create the Enemy

    var distance = 100;
    var marginLeft = 250;


    var marginTop = -200;
    var enemiesSpeed = .3;


    for (var i = 0; i < 24; ++i) {
        enemies.push(new Enemy(enemyObjects.tf.image));
        enemies[enemies.length-1].x = marginLeft + (i % Game.enemiesPerRow) * distance;
        //enemies[i].y = Hud.height + marginTop - Math.random() * (1000) ;
        enemies[enemies.length-1].y = Hud.height + marginTop + 80 * parseInt(i / Game.enemiesPerRow);
        enemies[enemies.length-1].autoShootTimer = Math.random() * enemies[i].autoShootInterval;
        enemies[enemies.length-1].vy = enemiesSpeed;
        enemies[enemies.length-1].friction = 1; // no friction
        enemies[enemies.length-1].explosion.frameSizeX = enemies[enemies.length-1].img.width; // adapt explosion to size
        enemies[enemies.length-1].explosion.frameSizeY = enemies[enemies.length-1].explosion.frameSizeX; // adapt explosion to size
        for (var j = 0; j < enemyObjects.tf.collision.length; ++j) {
            enemies[enemies.length-1].collisionArea.push(enemyObjects.tf.collision[j]);
        }
    }

    enemies.push(new Enemy(enemyObjects.sd.image));
    enemies[enemies.length-1].life = 400;
    enemies[enemies.length-1].x = 40;
    enemies[enemies.length-1].y = -300;
    enemies[enemies.length-1].bullet.color = "#0021DE";
    enemies[enemies.length-1].bullet.acc = 900;
    enemies[enemies.length-1].vy = enemiesSpeed;
    enemies[enemies.length-1].friction = 1; // no friction
    enemies[enemies.length-1].autoShootInterval = Game.enemiesShootInterval * .1;
    enemies[enemies.length-1].autoShootTimer = Math.random() * enemies[0].autoShootInterval;
    enemies[enemies.length-1].explosion.frameSizeX = enemies[enemies.length-1].img.width; // adapt explosion to size
    enemies[enemies.length-1].explosion.frameSizeY = enemies[enemies.length-1].explosion.frameSize; // adapt explosion to size
    for (var j = 0; j < enemyObjects.sd.collision.length; ++j)
        enemies[enemies.length-1].collisionArea.push(enemyObjects.sd.collision[j]);

    enemies.push(new Enemy(enemyObjects.sd.image));
    enemies[enemies.length-1].life = 400;
    enemies[enemies.length-1].x = 1000;
    enemies[enemies.length-1].y = -300;
    enemies[enemies.length-1].bullet.color = "#0021DE";
    enemies[enemies.length-1].bullet.acc = 900;
    enemies[enemies.length-1].vy = enemiesSpeed;
    enemies[enemies.length-1].friction = 1; // no friction
    enemies[enemies.length-1].autoShootInterval = Game.enemiesShootInterval * .1;
    enemies[enemies.length-1].autoShootTimer = Math.random() * enemies[0].autoShootInterval;
    enemies[enemies.length-1].explosion.frameSizeX = enemies[enemies.length-1].img.width; // adapt explosion to size
    enemies[enemies.length-1].explosion.frameSizeY = enemies[enemies.length-1].explosion.frameSize; // adapt explosion to size
    for (var j = 0; j < enemyObjects.sd.collision.length; ++j)
        enemies[enemies.length-1].collisionArea.push(enemyObjects.sd.collision[j]);


    enemies.push(new Enemy(enemyObjects.ds.image));
    enemies[enemies.length - 1].life = 400;
    enemies[enemies.length - 1].x = Game.stage.width / 2 - enemies[enemies.length - 1].img.width / 2;
    enemies[enemies.length - 1].y = -600;
    enemies[enemies.length - 1].bullet.color = "#0021DE";
    enemies[enemies.length - 1].bullet.acc = 900;
    enemies[enemies.length - 1].vy = enemiesSpeed;
    enemies[enemies.length - 1].friction = 1; // no friction
    enemies[enemies.length - 1].autoShootInterval = Game.enemiesShootInterval * .5;
    enemies[enemies.length - 1].autoShootTimer = Math.random() * Game.enemiesShootInterval * .5;
    enemies[enemies.length-1].explosion.frameSizeX = enemies[enemies.length-1].img.width; // adapt explosion to size
    enemies[enemies.length-1].explosion.frameSizeY = enemies[enemies.length-1].explosion.frameSize; // adapt explosion to size
    //enemies[enemies[enemies.length-1]-1].collisionArea = enemies[0].collisionArea;
    for (var j = 0; j < enemyObjects.ds.collision.length; ++j)
        enemies[enemies.length - 1].collisionArea.push(enemyObjects.ds.collision[j]);


    Game.numEnemies = enemies.length; // update Game variable

    for (var i = 0; i < 80 ; ++i) {
        obstacles.push(new Item(enemyObjects.as.image));
        obstacles[obstacles.length-1].img.loop = true;
        obstacles[obstacles.length-1].img.startAnim();
        obstacles[obstacles.length-1].isAlive = true;
        obstacles[obstacles.length-1].life = 10;
        obstacles[obstacles.length-1].x = 90 + Math.random() * 1000;
        obstacles[obstacles.length-1].y = -2000 + Math.random() * -40000;
        obstacles[obstacles.length-1].vy = 4 + Math.random()*2;
        obstacles[obstacles.length-1].friction = 1; // no friction
        for (var j = 0; j < enemyObjects.as.collision.length; ++j) {
            obstacles[obstacles.length-1].collisionArea.push(enemyObjects.as.collision[j]);
        }
    }

    // Create the Ship

    initPlayer(playerObjects.xw);

    t = Date.now(); // initialize value of t

    // Key press detection
    window.addEventListener('keyup', function (event) {
        Key.onKeyup(event);
    }, false);
    window.addEventListener('keydown', function (event) {
        Key.onKeydown(event);
    }, false);
    Hud.drawHud(context);
    Key.initOnScreenKeys();

    animFrame();
};

function animFrame() {
    setTimeout(function () {
        requestAnimationFrame(animFrame, canvas);
        Refresh();
    }, 1000 / 60);
}

function checkCollisions() {
    // Player-bullet vs Enemy collisions

    for(var key in player.bullets) {

        if (player.bullets[key].isAlive) {
            for (var i = 0; i < Game.numEnemies; ++i) {
                if (enemies[i].isAlive) {
                    var area = {};
                    for (var k = 0; k < enemies[i].collisionArea.length; ++k) {
                        area = {type: enemies[i].collisionArea[k].type, points: []};
                        for (var j = 0; j < enemies[i].collisionArea[k].points.length; ++j) {
                            area.points.push({ x: enemies[i].x + enemies[i].collisionArea[k].points[j].x, y: enemies[i].y + enemies[i].collisionArea[k].points[j].y});
                        }
                        if (collision.checkCollision({x: player.bullets[key].x, y: player.bullets[key].y}, area)) {
                            enemies[i].life -= player.bullets[key].life;
                            if (enemies[i].life <= 0) {
                                //enemies[i].isAlive = false;
                                /*enemies[i].explosion.frameSizeX = enemies[i].img.height;
                                 enemies[i].explosion.frameSizeY = enemies[i].explosion.frameSizeX;
                                 */
                                enemies[i].explosion.drawSizeX = enemies[i].img.height;
                                enemies[i].explosion.drawSizeY = enemies[i].explosion.drawSizeX;
                                enemies[i].destroy();
                                if (++points == Game.numEnemies) gameOver();
                            }
                            player.bullets[key].isAlive = false;
                        }
                    }
                }
            }

    }

        for (var i = 0; i < obstacles.length; ++i) {
            if (obstacles[i].isAlive) {
                var area = {};
                for (var k = 0; k < obstacles[i].collisionArea.length; ++k) {
                    area = {type: obstacles[i].collisionArea[k].type, points: []};
                    for (var j = 0; j < obstacles[i].collisionArea[k].points.length; ++j) {
                        area.points.push({ x: obstacles[i].x + obstacles[i].collisionArea[k].points[j].x, y: obstacles[i].y + obstacles[i].collisionArea[k].points[j].y});
                    }
                    if (collision.checkCollision({x: player.bullets[key].x, y: player.bullets[key].y}, area)) {
                        obstacles[i].life -= player.bullets[key].life;
                        if (obstacles[i].life <= 0) {
                           if(Math.random() > .8){
                                var bonusId = Date.now();
                                bonuses[bonusId] = BonusFactory.createBonus("rand",obstacles[i].x,obstacles[i].y);
                                if(bonuses[bonusId] == -1 ) delete bonuses[bonusId];
                           }
                           obstacles[i].destroy();
                        }
                        player.bullets[key].isAlive = false;
                    }

                }
            }

        }
    }

    for (var i = 0; i < obstacles.length; ++i) {
        if (obstacles[i].isAlive) {
            var area = {};
            for (var k = 0; k < obstacles[i].collisionArea.length; ++k) {
                area = {type: obstacles[i].collisionArea[k].type, points: []};
                for (var j = 0; j < obstacles[i].collisionArea[k].points.length; ++j) {
                    area.points.push({ x: obstacles[i].x + obstacles[i].collisionArea[k].points[j].x, y: obstacles[i].y + obstacles[i].collisionArea[k].points[j].y});
                }
                for (var j = 0; j < player.collisionPoints.length; ++j) {
                    if (collision.checkCollision({x: player.x + player.collisionPoints[j].x, y: player.y + player.collisionPoints[j].y}, area)) {
                        obstacles[i].type;
                        console.log("type: "+type + " , val:" +obstacles[i][type]);
                        player.shields -= obstacles[i].life;
                        if( player.shields < 0 ) {
                            player.life += player.shields;
                            if(player.life <= 0) player.destroy();
                        };
                        obstacles[i].life = 0;
                        obstacles[i].destroy();
                        i =  obstacles[i].collisionArea.length; // exit loop
                        j = player.collisionPoints.length;
                    }
                }
            }
        }
    }


    for(var key in bonuses) {
        if (bonuses[key].isAlive) {
            var area = {};
            for (var k = 0; k < bonuses[key].collisionArea.length; ++k) {
                area = {type: bonuses[key].collisionArea[k].type, points: []};
                for (var j = 0; j < bonuses[key].collisionArea[k].points.length; ++j) {
                    area.points.push({ x: bonuses[key].x + bonuses[key].collisionArea[k].points[j].x, y: bonuses[key].y + bonuses[key].collisionArea[k].points[j].y});
                }
                for (var j = 0; j < player.collisionPoints.length; ++j) {
                    if (collision.checkCollision({x: player.x + player.collisionPoints[j].x, y: player.y + player.collisionPoints[j].y}, area)) {
                        var type =  bonuses[key].type;
                        console.log("type: "+type + " , val:" +bonuses[key][type]);
                        player[type] -= bonuses[key][type];
                        if( type == "shields" && player.shields > 100) player.shields = 100;
                        else if( type == "life" && player.life > 100) player.life = 100;
                        else if( type == "firerate" && player.firerate > 700) player.firerate = 700;
                        bonuses[key].destroy();
                        i =   bonuses[key].collisionArea.length; // exit loop
                        j = player.collisionPoints.length;
                    }
                }
            }
        }
    }

}

function Refresh() {
    var dt = (Date.now() - t) / 1000; // time elapsed in seconds since last call
    t = Date.now(); // reset t
    context.clearRect(0, 0, canvas.width, canvas.height);

    checkCollisions();
    var area = {};
    // Refresh the Enemies
    for (var i = 0; i < Game.numEnemies; ++i) {
        if (enemies[i].isAlive) {
            enemies[i].y += enemies[i].vy;
            if(enemies[i].y>(-1 * enemies[i].img.height ) && enemies[i].y<Game.stage.height) {
                // calculate autoshoot timers and draw enemy ships only if they are displayed
                if (!enemies[i].bullet.isAlive) {
                    enemies[i].autoShootTimer -= dt;
                    if (enemies[i].autoShootTimer <= 0) {
                        enemies[i].shoot();
                        enemies[i].autoShootTimer = Math.random() * Game.enemiesShootInterval; // reset timer
                    }
                } else {
                    for (var k = 0; k < player.collisionArea.length; ++k) {
                        area = {type: player.collisionArea[k].type, points: []};
                        for (var j = 0; j < player.collisionArea[k].points.length; ++j) {
                            area.points.push({ x: player.x + player.collisionArea[k].points[j].x, y: player.y + player.collisionArea[k].points[j].y});
                        }
                        if (collision.checkCollision({x: enemies[i].bullet.x, y: enemies[i].bullet.y}, area)) {
                            if (player.shields > 0) {
                                player.shields -= enemies[i].bullet.life;
                                player.shield.startAnim();
                            }
                            else player.life -= enemies[i].bullet.life;
                            if (player.life <= 0) {
                                player.destroy();
                                gameOver();
                            }
                            enemies[i].bullet.isAlive = false;
                        }
                    }
                }
                enemies[i].draw(context);
            }
            enemies[i].updatePos(dt);
        } else if (enemies[i].isExploding) {
            enemies[i].explosion.draw(context);
        }
    }

    // Refresh the Enemies
    for (var i = 0; i < obstacles.length; ++i) {
        if (obstacles[i].isAlive) {
            obstacles[i].y += obstacles[i].vy;
            if(obstacles[i].y>(-1 * obstacles[i].img.img.height ) && obstacles[i].y<Game.stage.height) {
                obstacles[i].draw(context);
            }
            obstacles[i].updatePos(dt);
        } else if (obstacles[i].isExploding) {
            obstacles[i].explosion.draw(context);
        }
    }

    // Bonus Objects
    for(var key in bonuses) {
        if (bonuses[key].isAlive) {
            bonuses[key].y += bonuses[key].vy;
            if(bonuses[key].y>(-1 * bonuses[key].img.img.height ) && bonuses[key].y<Game.stage.height) {
                bonuses[key].draw(context);
            }
            bonuses[key].updatePos(dt);
        } else if (bonuses[key].isExploding) {
            bonuses[key].explosion.draw(context);
            if(bonuses[key].explosion.getCurrentFrame() >= bonuses[key].explosion.getNumFrames() ){
                delete bonuses[key];
            }
        }
    }

    // Refresh the Ship
    if (player.isAlive) {
        //document.getElementById("key-debug").innerHTML = ' x: ' + Key.analogX + ', y: ' + Key.analogY ;
        player.move(Key.analogX,Key.analogY);
        player.draw(context);
        player.updatePos(dt);
    } else if (player.isExploding) {
        player.explosion.draw(context);
    }

    Hud.drawHud(context);

};

var controlsHandler = function () {
    if (Key.isDown(Key.UP)) player.move(0,-1);
    if (Key.isDown(Key.LEFT)) player.move(-1,0);
    if (Key.isDown(Key.DOWN)) player.move(0,1);
    if (Key.isDown(Key.RIGHT)) player.move(1,0);
    //if (Key.isDown(Key.SPACE)) if (!this.bullet.isAlive) player.shoot();
    if (Key.isDown(Key.SPACE)) player.shoot();
}

function gameOver() {
    setTimeout(function () {
        location.reload(false)
    }, 2000);
}

function loadContent() {

    loadJSON("ships.json", function (response) {
        var jsonresponse = JSON.parse(response);
        enemyObjects = jsonresponse.enemies;
        playerObjects = jsonresponse.players;
        init();
    });

}

function initPlayer(asset){
    var x ;
    var y ;

    if(player && player.x != 0) x = player.x;
     else x = playerSpawnX;
    if(player && player.y != 0) y = player.y;
     else y = playerSpawnY;

    player = new Player(asset.image);
    player.x = x;
    player.y = y;
    player.img.x = player.x;
    player.img.y = player.y;
    for (var j = 0; j < asset.collision.length; ++j)
        player.collisionArea.push(asset.collision[j]);
    for (var j = 0; j < asset.collisionPoints.length; ++j)
        player.collisionPoints.push(asset.collisionPoints[j]);

    player.img.onload = function () {
        player.draw(context);
    }
    player.acc = asset.acc;
    player.friction = asset.friction;
    player.shields = asset.shields;
    player.firerate = asset.fireRate;
    player.bulletClass = {life : asset.bulletLife , acc : asset.bulletAcc};
    player.handleControls = controlsHandler;
}

function clone(obj) {
    // Deep copy
    var copy = jQuery.extend(true, {}, obj);
    return copy;
}