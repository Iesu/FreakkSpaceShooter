/**
  Created by Freakk on 26/12/2014.
  points are passed in the form {x: 0, y:0}
 */

var collision = {

    pointInRectangle: function(p,a,d){
        // for straight rectangles, rotated rectangles will be treated as a pair of triangles
        return p.x > a.x && p.x < d.x && p.y > a.y && p.y < d.y;
    },
    pointInTriangle: function(p,a,b,c){

        function fAB() {
            return (eval((p.y-a.y)*
                (b.x-a.x) -
                (p.x-a.x)*
                (b.y-a.y)))
        }
        function fBC() {
            return (eval((p.y-b.y)*
                (c.x-b.x) -
                (p.x-b.x)*
                (c.y-b.y)))
        }
        function fCA() {
            return (eval((p.y-c.y)*
                (a.x-c.x) -
                (p.x-c.x)*
                (a.y-c.y)))
        }
        
        if (fAB()*fBC()>0 && fBC()*fCA()>0)
        return true;

        return false;
    },
    pointInEllipse: function(p,el_left, el_top){

            var center = {x: el_top.x, y: el_left.y} ;
            var _xRadius = Math.abs(el_top.x - el_left.x);
            var _yRadius = Math.abs(el_top.y - el_left.y);


            if (_xRadius <= 0.0 || _yRadius <= 0.0)
                return false;
            /* This is a more general form of the circle equation
             *
             * X^2/a^2 + Y^2/b^2 <= 1
             */

            var  normalized = {x: p.x - center.x, y: p.y - center.y};

            return ((normalized.x * normalized.x)
                / (_xRadius * _xRadius)) + ((normalized.y * normalized.y) / (_yRadius * _yRadius))
                <= 1.0;
    },

    checkCollision: function(p,area){
        if(area.type=="rectangle"){
            return this.pointInRectangle(p,area.points[0],area.points[1]);
        }
        else if(area.type=="triangle"){
            return this.pointInTriangle(p,area.points[0],area.points[1],area.points[2]);
        }
        if(area.type=="ellipse"){
            return this.pointInEllipse(p,area.points[0],area.points[1]);
        }
        return false;
    }
}