/**
 * Created by Freakk on 01/01/2015.
 */

function Player(image){
    Ship.call(this);
    /*
    this.img = new Image();
    this.img.src = image;
    */
    self = this;
    this.img = new Sprite(image);
    //this.shield = new Sprite("images/shield.png");
    this.shield = new Sprite("images/shield_ball_fast.png");
    this.t = new Date().getTime(); // initialize value of t;
    this.shield.img.onload = function(){
        // sprite image has to be a row of square frames
        self.shield.frameStartX = 0;
        self.shield.frameStartY = 0 ;
        self.shield.frameSizeX = self.shield.img.height;
        self.shield.frameSizeY = self.shield.frameSizeX; // force square size
        //self.shield.drawSizeX = self.img.img.width > self.img.img.heigth ? self.img.img.width : self.img.img.height; // force square size choosing the largest value
        self.shield.drawSizeX = self.shield.frameSizeX;
        self.shield.drawSizeY =  self.shield.drawSizeX;
        self.shield.numFrames = parseInt(self.shield.img.width/self.shield.img.height);
    };
    this.img.isAlive = true;
    this.img.loop = false;
    var self = this;
    this.bullets = {};
}

// Inherit from the parent Ship class
Player.prototype = Object.create(Ship.prototype);
Player.prototype.constructor = Player;

Player.prototype.shoot = function () {
    // Override Ship method shoot to place the bullet on the down size of the ship
   var dt = Date.now() - this.t;  //time elapsed in milliseconds since last call
    if(dt > 60000/this.firerate ) {
        this.t = Date.now();
        var id = t;
        this.bullets[id] = new Bullet();
        this.bullets[id].life = this.bulletClass.life;
        this.bullets[id].acc = this.bulletClass.acc;
        this.bullets[id].fire(this.x + this.img.frameSizeX / 2, this.y + this.img.frameSizeY);
    }
}

Player.prototype.updatePos = function (dt) {
    this.handleControls();

    // Stage Borders Collisions
    var stoppedX = false;
    var stoppedY = false;

    if(this.x < 0) {
        this.x = 0;
        stoppedX = true;
    }
    else if (this.x > (Game.stage.width-this.img.frameSizeX)) {
        this.x = Game.stage.width-this.img.frameSizeX;
        stoppedX = true;
    }

    if(this.y < 0) {
        this.y = 0;
        stoppedY = true;
    }
    else if (this.y > (Game.stage.height-this.img.frameSizeY)) {
        this.y = Game.stage.height-this.img.frameSizeY;
        stoppedY = true;
    }
    if(stoppedX){
        this.vx *= -.4;
    } else  this.vx *= this.friction;
    if(stoppedY){
        this.vy *= -.4;
    } else  this.vy *= this.friction;

    // Update Coordinates
    this.x += this.vx * dt;
    this.y += this.vy * dt;
    this.img.x = this.x;
    this.img.y = this.y+10;
    //this.bullet.updatePos(dt);
    for(var key in this.bullets) {
        if(!this.bullets[key].isAlive || this.bullets[key].y<0) delete this.bullets[key];
        else this.bullets[key].updatePos(dt);
    }
}

Player.prototype.move = function (intensX, intensY) {
    // Sprite Animation
    if(intensX>0) {
        if(--this.img.currFrame < 0) this.img.currFrame = 0;
        if (intensX > 1) intensX = 1;
    } else if(intensX<0) {
        if(++this.img.currFrame >= this.img.numFrames-1) this.img.currFrame = (this.img.numFrames-1);
        if (intensX < -1) intensX = -1;
    }
    if(intensY>1) intensY=1;

    // Position
    this.vx += this.acc * intensX ;
    this.vy += this.acc * intensY ;

}

Player.prototype.draw = function (context) {
    if(this.isAlive){
        this.img.draw(context);

        //if(this.bullet.isAlive) this.bullet.draw(context);

        //Object.keys(this.bullets).length;
        for(var key in this.bullets) {
            this.bullets[key].draw(context);
        }

        if(Game.debug && this.collisionArea.length>0) {
            this.drawCollisionArea(context);
        }
        if(this.shield.isAlive){
            var shieldOffset = (this.img.img.height - this.shield.img.height)/2;
            this.shield.x = this.x + shieldOffset;
            this.shield.y = this.y + shieldOffset;
            this.shield.draw(context);
        }
    } else if(this.isExploding){
        this.explosion.draw(context);
    }
}