/**
 * Created by Freakk on 28/12/2014.
 */

var enemyObjects;
var enemyImages = {};
var levelContent;
var bg_canvas;
var bg_context;
var bg_img;

window.onload = function(){
    bg_canvas = document.getElementById('background');
    bg_context = bg_canvas.getContext('2d');
    bg_img = new Image();
    bg_img.onload = function () {
        bg_context.drawImage(bg_img, 0, 0);
    }


    levelCanvas = document.getElementById("level");
    levelCanvas.setAttribute("style","background:#222; width: 1280px; height: 1280px");
    loadJSON("ships.json", function (response) {
        var jsonresponse = JSON.parse(response);
        enemyObjects = jsonresponse.enemies;
        var lastEntry;
        for(var prop in enemyObjects) {
            if(enemyObjects.hasOwnProperty(prop))
                console.log("prop: "+prop);
                enemyImages[prop] = new Image();
                enemyImages[prop].src = enemyObjects[prop].image;
            lastEntry = prop;
        }
        enemyImages[lastEntry].onload = function(){ loadJSON("levels/level_1.json", loadLevel);} ;

    });


}

function drawLevel(context){
    for(var prop in levelContent.enemies) {
        context.drawImage(enemyImages[prop], levelContent.enemies[i][1], levelContent.enemies[i][2]);
    }
}

function loadLevel(response) {
    var jsonresponse = JSON.parse(response);
    levelContent = jsonresponse.level;
   // bg_img.src = levelContent.background;
    console.log(levelContent);
    context = levelCanvas.getContext('2d');
    drawLevel( context );
}
