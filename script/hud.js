/**
 * Created by Freakk on 22/12/2014.
 */
var Hud = {
    bgColor: "rgba(0,0,0,0)",
    borderColor: "rgba(93,186,252,.8)",
    height: 40,
    drawHud: function (context) {
        /*
        context.beginPath();
        context.strokeStyle = "rgba(0,0,0,0.2)";
        context.fillStyle = "rgba(0,0,0,0.2)";
        context.rect(0,0,Game.stage.width,this.height);
        //context.stroke();
        context.fill();
*/
        // LIFE
        this.cross(context, 10,10,21, this.borderColor, this.borderColor);
        if(player.life>0) this.roundRect(context, 40,10,200*player.life/100,20,5, "rgba(255,0,0,0.8)", "rgba(255,0,0,0.8)");
        this.roundRect(context, 40,10,200,20,5, this.borderColor, this.bgColor);
        // SHIELDS
        this.shield(context, 270,10,21, this.borderColor, this.borderColor);
        if(player.shields>0) this.roundRect(context, 300,10,200*player.shields/100,20,5, this.borderColor, this.borderColor);
        this.roundRect(context, 300,10,200,20,5, this.borderColor, this.bgColor);
    },
    cross: function(ctx, x, y, size, stroke, fill){
        ctx.beginPath();
        ctx.strokeStyle = stroke;
        ctx.lineWidth = "2";
        ctx.fillStyle = fill;
        var step = size/3;
        ctx.moveTo(x+step,y);
        ctx.lineTo(x+2*step,y);
        ctx.lineTo(x+2*step,y+step);
        ctx.lineTo(x+3*step,y+step);
        ctx.lineTo(x+3*step,y+2*step);
        ctx.lineTo(x+2*step,y+2*step);
        ctx.lineTo(x+2*step,y+3*step);
        ctx.lineTo(x+step,y+3*step);
        ctx.lineTo(x+step,y+2*step);
        ctx.lineTo(x,y+2*step);
        ctx.lineTo(x,y+step);
        ctx.lineTo(x+step,y+step);
        ctx.closePath();
        ctx.shadowColor = stroke;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 0;
        ctx.shadowBlur = 16;
        ctx.stroke();
        ctx.shadowBlur = 0;
        ctx.fill();
    },
    shield: function(ctx, x, y, size, stroke, fill){
        ctx.beginPath();
        ctx.strokeStyle = stroke;
        ctx.lineWidth = "2";
        ctx.fillStyle = fill;
        var step = size/2;
        ctx.moveTo(x,y);
        ctx.lineTo(x+size,y);
        ctx.lineTo(x+size,y+step);
        ctx.lineTo(x+size,y+step);
        ctx.lineTo(x+step,y+size);
        ctx.lineTo(x,y+step);
        ctx.closePath();
        ctx.shadowColor = this.borderColor;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 0;
        ctx.shadowBlur = 16;
        ctx.stroke();
        ctx.shadowBlur = 0;
        ctx.fill();
    },
    roundRect: function (ctx, x, y, w, h, radius,stroke, fill) {
        var r = x + w;
        var b = y + h;
        ctx.beginPath();
        ctx.strokeStyle = stroke;
        ctx.lineWidth = "2";
        ctx.fillStyle = fill;
        ctx.moveTo(x + radius, y);
        ctx.lineTo(r - radius, y);
        ctx.quadraticCurveTo(r, y, r, y + radius);
        ctx.lineTo(r, y + h - radius);
        ctx.quadraticCurveTo(r, b, r - radius, b);
        ctx.lineTo(x + radius, b);
        ctx.quadraticCurveTo(x, b, x, b - radius);
        ctx.lineTo(x, y + radius);
        ctx.quadraticCurveTo(x, y, x + radius, y);
        ctx.closePath();
        ctx.shadowColor = stroke;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 0;
        ctx.shadowBlur = 16;
        ctx.stroke();
        ctx.shadowBlur = 0;
        ctx.fill();
    }
}
