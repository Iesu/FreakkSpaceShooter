/**
 * Created by Freakk on 01/01/2015.
 */

function Enemy(image){
    Ship.call(this);
    this.img = new Image();
    this.img.src = image;
    this.bullet.color = "#FF0000";
    this.bullet.acc = 800;
    this.autoShootInterval = Game.enemiesShootInterval;
    this.autoShootTimer = Math.random() * this.autoShootInterval ;
}

// Inherit from the parent Ship class
Enemy.prototype = Object.create(Ship.prototype);
Enemy.prototype.constructor = Enemy;


Enemy.prototype.shoot = function shoot() {
    // Override Ship method shoot to place the bullet on the down size of the ship
    this.bullet.fire(this.x + this.img.width / 2, this.y + this.img.height);
}