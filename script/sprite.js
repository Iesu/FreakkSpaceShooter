/**
 * Created by Freakk on 22/12/2014.
 */
function Sprite(image) {
    var self = this;
    this.img = new Image();
    this.img.src = image;
    this.numFrames = 0;
    this.currFrame = 0;
/*    this.frameSizeX = 0; // size of the sprite frame
    this.frameSizeY = 0;
    this.drawSizeX = 0; // size of the drawing area (it's possible to resize the sprite at drawing time)
    this.drawSizeY = 0;
    this.frameStartX = 0;
    this.frameStartY = 0;*/
    this.x = 0;
    this.y = 0;
    this.vx = 0;
    this.vy = 0;
    this.acc = 40;
    this.friction = .95;
    this.life = 100;
    this.isAlive = false;
    this.isPlaying = false;
    this.loop = false;
    this.collisionArea = []; // area of polygons in the form {type: "rect/triang/ell", points: [{x:0,y:0}, ...]}
    this.opacity = 1;
    this.img.onload = function(){
        // sprite image has to be a row of square frames
        self.frameStartX = 0;
        self.frameStartY = 0;
        self.frameSizeX = self.img.height;
        self.frameSizeY = self.frameSizeX; // force square size
        self.drawSizeX = self.img.width > self.img.heigth ? self.img.width : self.img.height; // force square size choosing the largest value
        self.drawSizeY =  self.drawSizeX;
        self.numFrames = parseInt(self.img.width/self.img.height);
        self.currFrame = parseInt(self.numFrames/2);
    };
}

Sprite.prototype.startAnim = function () {
    this.isAlive = true;
    this.isPlaying = true;
    this.currFrame = 0;
    //this.frameStartX = 0;
}

Sprite.prototype.draw = function (context) {
    this.frameStartX = this.frameSizeX * (this.currFrame);
        if(this.opacity<1){
            context.save();
            context.globalAlpha = this.opacity;
            context.drawImage(this.img,  this.frameStartX,  this.frameStartY,this.frameSizeX,this.frameSizeY, this.x, this.y, this.drawSizeX,this.drawSizeY);
            context.restore();
        } else context.drawImage(this.img,  this.frameStartX,  this.frameStartY,this.frameSizeX,this.frameSizeY, this.x, this.y, this.drawSizeX,this.drawSizeY);
    if( this.isPlaying ){
        if(++this.currFrame == this.numFrames ) { // reached end of animation
            if(!this.loop) {
                this.isPlaying = false;
                this.isAlive = false;
            } else this.currFrame = 0;
        }
    }
    if(Game.debug && this.collisionArea.length>0) {
        this.drawCollisionArea(context);
    }
}

Sprite.prototype.drawCollisionArea = function (context) {
    context.strokeStyle = 'red';
    context.lineWidth=2;
    for(var i=0; i< this.collisionArea.length; ++i) {
        switch (this.collisionArea[i].type) {
            case "rectangle":
                context.beginPath();
                context.moveTo(this.x + this.collisionArea[i].points[0].x, this.y + this.collisionArea[i].points[0].y);
                context.lineTo(this.x + this.collisionArea[i].points[1].x, this.y + this.collisionArea[i].points[0].y);
                context.lineTo(this.x + this.collisionArea[i].points[1].x, this.y + this.collisionArea[i].points[1].y);
                context.lineTo(this.x + this.collisionArea[i].points[0].x, this.y + this.collisionArea[i].points[1].y);
                context.closePath();
                context.stroke();
                break;
            case "triangle":
                context.beginPath();
                context.moveTo(this.x + this.collisionArea[i].points[0].x, this.y + this.collisionArea[i].points[0].y);
                context.lineTo(this.x + this.collisionArea[i].points[1].x, this.y + this.collisionArea[i].points[1].y);
                context.lineTo(this.x + this.collisionArea[i].points[2].x, this.y + this.collisionArea[i].points[2].y);
                context.closePath();
                context.stroke();
                break;
            case "ellipse":
            function drawOval(x, y, rw, rh)
            {
                context.save();
                context.scale(1,  rh/rw);
                context.beginPath();
                context.arc(x, y, rw, 0, 2 * Math.PI);
                context.restore();
                //context.strokeStyle="orange";
                context.stroke();
            }
                var centerX = this.x + this.collisionArea[i].points[1].x;
                var centerY = this.y + this.collisionArea[i].points[0].y;
                var rw = Math.abs( this.collisionArea[i].points[1].x - this.collisionArea[i].points[0].x);
                var rh = Math.abs(this.collisionArea[i].points[0].y - this.collisionArea[i].points[1].y);
                drawOval(centerX,centerY,rw, rh);
                break;
        }
    }
}

Sprite.prototype.getCurrentFrame = function () { return this.currFrame; }

Sprite.prototype.getNumFrames = function () { return this.numFrames; }
