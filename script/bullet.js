function Bullet(timestamp) {
    this.timestamp = timestamp;
    this.color = "#00FF11";
    this.width = 4;
    this.height = 8;
    this.x = -1 * this.width;  // start drawing bullet outside of the screen
    this.y = -1 * this.height; // start drawing bullet outside of the screen
    this.vx = 0;
    this.vy = 0;
    this.acc = -900;
    this.friction = .99; // bullets start fast, then their speed decays
    this.isAlive = false;
    this.life = 20;
}


Bullet.prototype.fire = function( posX, posY ){
    this.x = posX;
    this.y = posY;
    this.vy = this.acc;
    this.isAlive = true;
}

Bullet.prototype.draw = function (context) {
    context.fillStyle = this.color;
    context.beginPath();
    context.moveTo(this.x, this.y);
    context.lineTo(this.x, this.y-this.height);
    context.lineWidth = 4;
    context.strokeStyle = this.color;
    context.lineCap = 'round';
    context.shadowColor = this.color // string
    context.shadowOffsetX = 0; // integer
    context.shadowOffsetY = 0; // integer
    context.shadowBlur = 16; // integer
    context.stroke();
    context.shadowBlur = 0; // integer
}


Bullet.prototype.updatePos = function (dt) {
    this.x += this.vx * dt;
    this.y += this.vy * dt;
    this.vx *= this.friction;
    this.vy *= this.friction;

    if (this.y < (-1 * this.height) || this.y > (Game.stage.height+this.height)){
        this.stop();
    }

}

Bullet.prototype.stop = function () {
    this.vx = this.vy = 0;
    this.isAlive = false;
}

