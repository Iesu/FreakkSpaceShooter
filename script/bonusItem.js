/**
 * Created by Freakk on 17/02/2015.
 */

var BonusFactory = {
    types: ["rand", "shields", "firerate", "life"],
    images: {
        "shields": "images/shield_obj_sprite.png",
        "firerate": "images/ammo_obj_sprite.png",
        "life": "images/health_obj_sprite.png"
    },
    createBonus: function BonusItem(t, xPos, yPos) {
        var self = this;
        var type = null;

        if( t == "rand"){
            type = self.types[parseInt(1 + Math.random()*(self.types.length-1))];
        }

        var bonusValid = false;
        for (var i = 0; i < this.types.length; ++i)
            if (this.types[i] == type) bonusValid = true;

        if (!bonusValid) {
            console.log("Error: BonusFactory type invalid");
            return;
        } else {
            var bonus = new Item(this.images[type]);
            bonus.img.loop = true;
            bonus.img.startAnim();
            bonus.isAlive = true;
            bonus.x = xPos;
            bonus.y = yPos;
            bonus.vy = 0;
            bonus.collisionArea.push({ "type": "rectangle", "points": [
                {"x": 0, "y": 0},
                {"x": bonus.img.img.height, "y": bonus.img.img.height}
            ]});
            bonus.explosion = new Sprite("images/bonus_explosion.png");
            bonus.type = type;
            switch (type) {
                case "shields":
                    bonus.shields = -50;
                    bonus.life = -20;
                    break;
                case "firerate":
                    bonus.firerate = -400;
                    bonus.life = -20;
                    break;
                case "life":
                    bonus.type = "life";
                    bonus.life = -50;
                    break;
            }
            return bonus;
        }

    }
}