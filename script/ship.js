function Ship(image) {
    this.img = new Image();
    this.img.src = image;
    this.x = 0;
    this.y = 0;
    this.vx = 0;
    this.vy = 0;
    this.acc = 40;
    this.friction = .95;
    this.bullet = new Bullet();
    this.bulletClass = {life : 20 , acc: 800};
    this.firerate = 180;
    this.life = 100;
    this.shields = 100;
    this.isAlive = true;
    this.isExploding = false;
    this.explosion = new Sprite("images/explosion.png");
    this.collisionArea = []; // area of polygons in the form {type: "rect/triang/ell", points: [{x:0,y:0}, ...]}
    this.collisionPoints = []; // array of points (usually 4) use for ship to objects/ships collisions
}

Ship.prototype.draw = function (context) {
       if(this.isAlive){
           context.drawImage(this.img, this.x, this.y);
           if(this.bullet.isAlive) this.bullet.draw(context);

           if(Game.debug && this.collisionArea.length>0) {
               this.drawCollisionArea(context);
           }

       } else if(this.isExploding){
           this.explosion.draw(context);
       }
}

Ship.prototype.drawCollisionArea = function (context) {
    context.strokeStyle = 'red';
    context.lineWidth=2;
    for(var i=0; i< this.collisionArea.length; ++i) {
        switch (this.collisionArea[i].type) {
            case "rectangle":
                context.beginPath();
                context.moveTo(this.x + this.collisionArea[i].points[0].x, this.y + this.collisionArea[i].points[0].y);
                context.lineTo(this.x + this.collisionArea[i].points[1].x, this.y + this.collisionArea[i].points[0].y);
                context.lineTo(this.x + this.collisionArea[i].points[1].x, this.y + this.collisionArea[i].points[1].y);
                context.lineTo(this.x + this.collisionArea[i].points[0].x, this.y + this.collisionArea[i].points[1].y);
                context.closePath();
                context.stroke();
                break;
            case "triangle":
                context.beginPath();
                context.moveTo(this.x + this.collisionArea[i].points[0].x, this.y + this.collisionArea[i].points[0].y);
                context.lineTo(this.x + this.collisionArea[i].points[1].x, this.y + this.collisionArea[i].points[1].y);
                context.lineTo(this.x + this.collisionArea[i].points[2].x, this.y + this.collisionArea[i].points[2].y);
                context.closePath();
                context.stroke();
                break;
            case "ellipse":
                function drawOval(x, y, rw, rh)
                {
                    context.save();
                    context.scale(1,  rh/rw);
                    context.beginPath();
                    context.arc(x, y, rw, 0, 2 * Math.PI);
                    context.restore();
                    //context.strokeStyle="orange";
                    context.stroke();
                }
                var centerX = this.x + this.collisionArea[i].points[1].x;
                var centerY = this.y + this.collisionArea[i].points[0].y;
                var rw = Math.abs( this.collisionArea[i].points[1].x - this.collisionArea[i].points[0].x);
                var rh = Math.abs(this.collisionArea[i].points[0].y - this.collisionArea[i].points[1].y);
                drawOval(centerX,centerY,rw, rh);
                break;
        }
    }

    for(var i=0; i< this.collisionPoints.length; ++i) {
        context.fillStyle = 'blue';
        var dotSize = 4;
        context.fillRect(this.x + this.collisionPoints[i].x - dotSize/2 , this.y+this.collisionPoints[i].y - dotSize/2,dotSize, dotSize);
    }
}

Ship.prototype.move = function (intensX, intensY) {
    if(intensX>1) intensX=1;
    if(intensY>1) intensY=1;
    this.vx += this.acc * intensX ;
    this.vy += this.acc * intensY ;
}

Ship.prototype.handleControls = function(){
    // Override me, Example:
    /*
     if (Key.isDown(Key.UP)) this.moveUp();
     if (Key.isDown(Key.LEFT)) this.moveLeft();
     if (Key.isDown(Key.DOWN)) this.moveDown();
     if (Key.isDown(Key.RIGHT)) this.moveRight();
     if (Key.isDown(Key.L_CTRL)) if(!this.bullet.isAlive) this.shoot();
    */
}

Ship.prototype.updatePos = function (dt) {
    this.handleControls();


    this.vx *= this.friction;
    this.vy *= this.friction;

    // Update Coordinates
    this.x += this.vx * dt;
    this.y += this.vy * dt;

    this.bullet.updatePos(dt);
}

Ship.prototype.shoot = function(){
    this.bullet.fire(this.x+this.img.width/2, this.y);
}

Ship.prototype.destroy = function(){
    this.isAlive = false;
    this.explosion.x = this.x;
    this.explosion.y = this.y;
    this.explosion.startAnim();
    this.explosion.isAlive = true;
    this.isExploding = true;
}
