HTML5 Space Shooter Game
========================

Space shooter game built using HTML5 canvas.

![alt tag](screenshot.jpg)

[Demo](http://www.freakk.net/html5/spaceshooter)

CONTROLS:

    ********
    *  PC  *
    ********
      ARROWS:   Movement
      SPACEBAR: Fire

      DEBUG Keys
      D:        Show Collision Polygons
      1:        Spawn Player 1
      2:        Spawn Player 2

    ************
    *  MOBILE  *
    ************
      Tap the Joystick button on the top-right corner of the screen to activate on-screen controls

## Test
npm start
open http://localhost:8080
